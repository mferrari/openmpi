# There can be multiple blocks of configuration data, chosen by
# compiler flags (using the compiler_args key to chose which block
# should be activated.  This can be useful for multilib builds.  See the
# multilib page at:
#    https://github.com/open-mpi/ompi/wiki/compilerwrapper3264
# for more information.

project=Open MPI
project_short=OMPI
version=4.1.4
language=C
compiler_env=CC
compiler_flags_env=CFLAGS
compiler=/scratch/dphpc/spack/lib/spack/env/gcc/gcc
preprocessor_flags=   
compiler_flags_prefix=
compiler_flags=-pthread 
linker_flags=-L/scratch/dphpc/spack/opt/spack/linux-centos7-sandybridge/gcc-12.2.0/hwloc-2.8.0-2dhvy46mkzq5qhpdystkknvgzqrjulxc/lib -L/scratch/dphpc/spack/opt/spack/linux-centos7-sandybridge/gcc-12.2.0/libevent-2.1.12-hn7j2sxe3jod2mtbcyep2oyrtbqijc2c/lib -L/scratch/dphpc/spack/opt/spack/linux-centos7-sandybridge/gcc-12.2.0/pmix-4.1.2-anwiu4uwal7ux5spjznf54x5rlrzgcox/lib  -Wl,-rpath,/scratch/dphpc/spack/opt/spack/linux-centos7-sandybridge/gcc-4.8.5/gcc-12.2.0-sp6uaabw2owtavpqvaju4z5pdwvphsba/lib/gcc/x86_64-pc-linux-gnu/12.2.0 -Wl,-rpath,/scratch/dphpc/spack/opt/spack/linux-centos7-sandybridge/gcc-4.8.5/gcc-12.2.0-sp6uaabw2owtavpqvaju4z5pdwvphsba/lib64 -Wl,-rpath -Wl,@{libdir}  -Wl,-rpath -Wl,/scratch/dphpc/spack/opt/spack/linux-centos7-sandybridge/gcc-12.2.0/hwloc-2.8.0-2dhvy46mkzq5qhpdystkknvgzqrjulxc/lib -Wl,-rpath -Wl,/scratch/dphpc/spack/opt/spack/linux-centos7-sandybridge/gcc-12.2.0/libevent-2.1.12-hn7j2sxe3jod2mtbcyep2oyrtbqijc2c/lib -Wl,-rpath -Wl,/scratch/dphpc/spack/opt/spack/linux-centos7-sandybridge/gcc-12.2.0/pmix-4.1.2-anwiu4uwal7ux5spjznf54x5rlrzgcox/lib 
# Note that per https://svn.open-mpi.org/trac/ompi/ticket/3422, we
# intentionally only link in the MPI libraries (ORTE, OPAL, etc. are
# pulled in implicitly) because we intend MPI applications to only use
# the MPI API.
libs=-lmpi
libs_static=-lmpi -lopen-rte -lopen-pal -lhwloc -ldl -levent_core -levent_pthreads -lpmix -lrt -lutil -lm  -lz
dyn_lib_file=libmpi.so
static_lib_file=libmpi.a
required_file=
includedir=${includedir}
libdir=${libdir}
