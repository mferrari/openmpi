/*
 * Copyright (c) 2004-2005 The Trustees of Indiana University and Indiana
 *                         University Research and Technology
 *                         Corporation.  All rights reserved.
 * Copyright (c) 2004-2005 The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 * Copyright (c) 2004-2005 High Performance Computing Center Stuttgart,
 *                         University of Stuttgart.  All rights reserved.
 * Copyright (c) 2004-2005 The Regents of the University of California.
 *                         All rights reserved.
 * Copyright (c) 2011 Cisco Systems, Inc.  All rights reserved.
 * Copyright (c) 2016      Research Organization for Information Science
 *                         and Technology (RIST). All rights reserved.
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 *
 * This file should be included by any file that needs full
 * version information for the OPAL project
 */

#ifndef OPAL_VERSIONS_H
#define OPAL_VERSIONS_H

#define OPAL_MAJOR_VERSION 4
#define OPAL_MINOR_VERSION 1
#define OPAL_RELEASE_VERSION 4
#define OPAL_GREEK_VERSION ""
#define OPAL_WANT_REPO_REV @OPAL_WANT_REPO_REV@
#define OPAL_REPO_REV "v4.1.4"
#ifdef OPAL_VERSION
/* If we included version.h, we want the real version, not the
   stripped (no-r number) verstion */
#undef OPAL_VERSION
#endif
#define OPAL_VERSION "4.1.4"
#define OPAL_CONFIGURE_CLI " \'--prefix=/scratch/dphpc/spack/opt/spack/linux-centos7-sandybridge/gcc-12.2.0/openmpi-4.1.4-s4kl6kl3u2uoy7yvvhfepnaw36b75arj\' \'--enable-shared\' \'--disable-silent-rules\' \'--disable-builtin-atomics\' \'--enable-static\' \'--enable-mpi1-compatibility\' \'--without-hcoll\' \'--without-cma\' \'--without-ofi\' \'--without-ucx\' \'--without-xpmem\' \'--without-psm2\' \'--without-fca\' \'--without-psm\' \'--without-mxm\' \'--without-verbs\' \'--without-knem\' \'--without-cray-xpmem\' \'--without-loadleveler\' \'--without-slurm\' \'--without-alps\' \'--without-tm\' \'--without-sge\' \'--without-lsf\' \'--disable-memchecker\' \'--with-libevent=/scratch/dphpc/spack/opt/spack/linux-centos7-sandybridge/gcc-12.2.0/libevent-2.1.12-hn7j2sxe3jod2mtbcyep2oyrtbqijc2c\' \'--with-pmix=/scratch/dphpc/spack/opt/spack/linux-centos7-sandybridge/gcc-12.2.0/pmix-4.1.2-anwiu4uwal7ux5spjznf54x5rlrzgcox\' \'--with-zlib=/scratch/dphpc/spack/opt/spack/linux-centos7-sandybridge/gcc-12.2.0/zlib-1.2.13-wobwbhgw5ctvw7nfe56ne6fkiowpqq6d\' \'--with-hwloc=/scratch/dphpc/spack/opt/spack/linux-centos7-sandybridge/gcc-12.2.0/hwloc-2.8.0-2dhvy46mkzq5qhpdystkknvgzqrjulxc\' \'--disable-java\' \'--disable-mpi-java\' \'--with-gpfs=no\' \'--without-cuda\' \'--enable-wrapper-rpath\' \'--disable-wrapper-runpath\' \'--disable-mpi-cxx\' \'--disable-cxx-exceptions\' \'--with-wrapper-ldflags=-Wl,-rpath,/scratch/dphpc/spack/opt/spack/linux-centos7-sandybridge/gcc-4.8.5/gcc-12.2.0-sp6uaabw2owtavpqvaju4z5pdwvphsba/lib/gcc/x86_64-pc-linux-gnu/12.2.0 -Wl,-rpath,/scratch/dphpc/spack/opt/spack/linux-centos7-sandybridge/gcc-4.8.5/gcc-12.2.0-sp6uaabw2owtavpqvaju4z5pdwvphsba/lib64\'"

#endif
