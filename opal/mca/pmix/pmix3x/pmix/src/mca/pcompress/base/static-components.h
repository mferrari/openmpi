/*
 * $HEADER$
 */
#if defined(c_plusplus) || defined(__cplusplus)
extern "C" {
#endif

extern const pmix_mca_base_component_t mca_pcompress_zlib_component;

const pmix_mca_base_component_t *mca_pcompress_base_static_components[] = {
  &mca_pcompress_zlib_component, 
  NULL
};

#if defined(c_plusplus) || defined(__cplusplus)
}
#endif

